Exercice Mysql

Questions

1- Afficher la liste des pays où Simplon est présent
2- Afficher la liste des fabriques de la ville de Dakar
3- Afficher la liste des pays ayant au moins 2 fabriques
4- Afficher le nombre d’apprenants par fabrique
5- Afficher la liste des apprenants ayant au moins un contrat
6- Ecrire l’ordre sql qui permet d’ajouter un champ ouvert à la table cohorte de type bouléen
7- Modifier le type du champ sexe en char1

Réponses

sudo mysql

show DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| dibor              |
| helene             |
| helenedb           |
| helenedibor        |
| mysql              |
| performance_schema |
| simplon            |
| sys                |
| wordpress          |



use simplon;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed

Pour montrer les tables contenues dans simplon
mysql> show tables;
+-------------------+
| Tables_in_simplon |
+-------------------+
| apprenant         |
| cohorte           |
| contrat           |
| fabrique          |
| pays              |
| ref_Fabrique      |
| referentiel       |
+-------------------+

1- Afficher la liste des pays où Simplon est présent

select * from pays left join fabrique on id_pays = fabrique.id_pays;
+----+---------------+---------+------+----------------------+----------------+-----------+----------------------------+---------+
| id | nom           | ville   | id   | nom                  | adresse        | tel       | email                      | id_pays |
+----+---------------+---------+------+----------------------+----------------+-----------+----------------------------+---------+
|  4 | Senegal       | Dakar   |    1 | Simplon Dakar        | Corniche Ouest | 338242927 | SIMPLONAUF@DAKAR           |       4 |
|  5 | Cote d'Ivoire | Abidjan |    1 | Simplon Dakar        | Corniche Ouest | 338242927 | SIMPLONAUF@DAKAR           |       4 |
|  7 | Mali          | Bamako  |    1 | Simplon Dakar        | Corniche Ouest | 338242927 | SIMPLONAUF@DAKAR           |       4 |



2- Afficher la liste des fabriques de la ville de Dakar

select nom from fabrique  where id_pays=4;
+----------------------+
| nom                  |
+----------------------+
| Simplon Dakar        |
| ORANGE ACADEMY DAKAR |
+----------------------+

3- Afficher la liste des pays ayant au moins 2 fabriques





4- Afficher le nombre d’apprenants par fabrique

select nombre_apprenant from fabrique;
ERROR 1054 (42S22): Unknown column 'nombre_apprenant' in 'field list'


5- Afficher la liste des référentiels par fabrique

select nom_ref from fabrique;
ERROR 1054 (42S22): Unknown column 'nom_ref' in 'field list'

Erreur car le champ liste des referentiels n'est pas prise en compte dans la table fabrique


6- Afficher la liste des apprenants ayant au moins un contrat

 select * from apprenant left join contrat on id_apprenant = contrat.id_apprenant and situation_professionnelle = 'en poste';
+----+--------+----------------+------------+----------------+------+-----------------------------+-----------+----------------+------------+-------------+------+---------------------------+-----------------------------------------+--------------+------------+------------+---------------+--------------+
| id | nom    | prenom         | statut     | date_naissance | sexe | email                       | tel       | id_referentiel | id_cohorte | id_fabrique | id   | situation_professionnelle | poste                                   | type_contrat | date_debut | date_fin   | nom_structure | id_apprenant |
+----+--------+----------------+------------+----------------+------+-----------------------------+-----------+----------------+------------+-------------+------+---------------------------+-----------------------------------------+--------------+------------+------------+---------------+--------------+
|  1 | Faye   | Hélène Dibor   | apprenante | 1992-06-12     |    0 | fayehelene12@gmail.com      | 778258010 |              1 |          1 |           1 |    1 | en poste                  | Ingénieur géologue référent digital     | CDD          | 2019-11-01 | 2022-01-01 | TOTAL EPC     |            1 |
|  2 | Ndao   | Mame Fati      | apprenante | 1998-02-15     |    0 | mfandao@gmail.com           | 778371788 |              2 |          1 |           1 |    1 | en poste                  | Développeur                             | CDD          | 2019-11-01 | 2022-01-01 | TOTAL EPC     |            1 |
|  3 | Niane  | Ami            | apprenante | 1991-07-07     |    0 | aminiane591@gmail.com       | 777660584 |              1 |          1 |           1 |    1 | en poste                  | référent digital                        | CDD          | 2019-11-01 | 2022-01-01 | TOTAL EPC     |            1 |
|  4 | Marigo | Mamadou        | apprenant  | 1994-05-25     |    1 | mamadoumarigo1994@gmail.com | 771268977 |              2 |          1 |           4 |    1 | en poste                  | Développeur                             | CDD          | 2019-11-01 |  2022-01-01 | TOTAL EPC     |            1 |



6- Ecrire l’ordre sql qui permet d’ajouter un champ ouvert à la table cohorte de type bouléen

ALTER table cohorte add ouvert boolean;


7- Modifier le type du champ sexe: mettre en char1 au lieu de booléen

ALTER table apprenant modify sexe CHAR(1);

Query OK, 4 rows affected (1.60 sec)
Records: 4  Duplicates: 0  Warnings: 0

